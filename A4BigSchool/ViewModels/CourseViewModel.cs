﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using A4BigSchool.Models; // Import thêm namespace của Category nếu cần

namespace A4BigSchool.ViewModels
{
    public class CourseViewModel
    {
        //[Required(ErrorMessage = "Place is required.")] 
        //public string Place { get; set; }
        [Required(ErrorMessage = "The Place field is required")]
        public string Place { get; set; }

        [Required]
        [FutureDate]
        public string Date { get; set; }

        //[Required(ErrorMessage = "Date is required.")]
        //[Display(Name = "Date")]
        //[DisplayFormat(DataFormatString = "{0:dd/MM/yyyy}", ApplyFormatInEditMode = true)]
        // public string Date { get; set; }




        [Required]
        [ValidTime]
        public string Time { get; set; }

        [Required(ErrorMessage = "Category is required.")]
        public byte Category { get; set; }

        public IEnumerable<Category> Categories { get; set; }

        public DateTime GetDateTime()
        {
            return NewMethod();
        }

        private DateTime NewMethod()
        {
            return DateTime.Parse(string.Format("{0} {1}", Date, Time));
        }
    }
}
