﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(A4BigSchool.Startup))]
namespace A4BigSchool
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
